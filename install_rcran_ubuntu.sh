#!/usr/bin/env bash

# script para la instalación de R en Ubuntu y derivadas

# CC By-SA 2020 Miguel Sevilla-Callejo

# extraido del código del script personal de instalación de
# programas en Ubuntu del repositorio de gitlab:
# https://gitlab.com/msevilla00/linux_scripts/-/blob/master/script_postinstalacion_ubuntu.sh

# actualizado a 29 de abril de 2022

## ACTIVACIÓN DE REPOSITORIO DE R

# R - última versión -- SOLO BIONIC y versión 4.2 --
#NUEVAS clave gpg para CRAN
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc

# COMENTAR si se quiere la versión 3.4 que está en repo oficial de bionic
# http://cran.es.r-project.org/
## Añadir el repositorio en /etc/apt/sources.list
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"

# (6a) Núcleo de R (http://cran.es.r-project.org/)
sudo apt install -y r-base r-base-dev r-base-html build-essential

# dependencias que me han surgido para instalar paquetes:
sudo apt install -y libxml2-dev libudunits2-dev

# NO instalar todos los paquetes de R listados en Ubuntu
#sudo apt install -y r-cran-*
# pues en el último repo de R solo se mantienen unos pocos (ver abajo).

# instalación de TODOS los paquetes mantenidos en repo extra
# en realidad todos deberían haberse instalado en el comando anterior.
# ver más info en: https://cran.r-project.org/bin/linux/ubuntu/
sudo apt install -y r-cran-boot r-cran-class r-cran-cluster \
r-cran-codetools r-cran-foreign r-cran-kernsmooth r-cran-lattice \
r-cran-mass r-cran-matrix r-cran-mgcv r-cran-nlme r-cran-nnet \
r-cran-rpart r-cran-spatial r-cran-survival

# solo el siguiente NO se instala con la base de más arriba
sudo apt install -y r-cran-rodbc

## INSTALAR RSTUDIO --> EN REVISIÓN

# satisfacer dependencias de RStudio
sudo apt install -y libjpeg62

# URL de la última versión de RStudio (64bits) a 29 de abril de 2022
# Primer intento de automatización de Rstudio Stable Latest
#url="https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2022.02.2-485-amd64.deb"

# Nuevo procedimiento para instalar la última versión estable de rstudio
# Buscaremos el nombre en el repo https://github.com/Thell/rstudio-latest-urls
# Ahí se actualizan diariamente los urls de las últimas versiones de rstudio
# Se tienen todas: daily, preview, stable
url=https://github.com/thell/rstudio-latest-urls/raw/master/latest.json

# Se requiere jq para manipular el json del repo
if ! command -v jq &> /dev/null
then
    echo "Required 'jq' command not found! installing 'jq'..."
    sudo apt-get install -y jq
fi
url=$(jq -r '.stable.desktop.bionic.rstudio' <(curl -s -L ${url}))

# Extraigo el nombre para hacer la descarga desde la web oficial. Ese repo pone
# las últimas versiones en servidor no oficial de rstudio
rsname=`basename "${url}"`
echo "Last stable version: ${rsname}"

#Descargo el fichero e instalo
echo "Downloading rstudio..." 
curl -# -o "/tmp/${rsname}" "https://download1.rstudio.org/desktop/bionic/amd64/${rsname}"

# instalar
sudo dpkg -i "/tmp/${rsname}"
#EOF
